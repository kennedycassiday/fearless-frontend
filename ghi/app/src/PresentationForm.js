import React, { useEffect, useState } from 'react';

function PresentationForm () {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
      }

    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
      }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
      }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
      }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference
        console.log(data);

        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            }
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
    }

    }

    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)

        }

    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Presenter Name" required type="text" id="presenter_name" name="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={email} onChange={handleEmailChange} placeholder="Presenter Email" required type="email" id="presenter_email" name="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company Name" type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={title} onChange={handleTitleChange} placeholder="Title" type="text" id="title" name="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value={synopsis} onChange={handleSynopsisChange} required type="text" id="synopsis" name="synopsis" className="form-control" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select value={conference} onChange={handleConferenceChange} required id="conference" name="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>{conference.name}</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}


export default PresentationForm;
