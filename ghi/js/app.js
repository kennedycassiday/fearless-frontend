function createCard(name, description, pictureUrl, dateStarts, dateEnds, location) {
    return `
    <div class="col">
      <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
            <small class="text-muted">${dateStarts} - ${dateEnds}</small>
        </div>
        </div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log('Response is bad')
      } else {
        const data = await response.json();
        let counter = 1
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const location = details.conference.location.name
                const description = details.conference.description;
                const start = new Date(details.conference.starts)
                const dateStarts = start.toLocaleDateString()
                const end = new Date(details.conference.ends)
                const dateEnds = end.toLocaleDateString()
                const pictureUrl = details.conference.location.picture_url;
                const html = createCard(name, description, pictureUrl, dateStarts, dateEnds, location);
                const column = document.querySelector('.row');
                column.innerHTML += html;

            }
        }

      }
    } catch (e) {
      console.log('error', e)
    }

  });
